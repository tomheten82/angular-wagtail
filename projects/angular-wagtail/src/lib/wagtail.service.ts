import { Injectable, Inject, PLATFORM_ID } from "@angular/core";
import {
  isPlatformServer,
  isPlatformBrowser,
  Location,
  DOCUMENT,
} from "@angular/common";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Router } from "@angular/router";
import {
  makeStateKey,
  TransferState,
  Meta,
  Title,
  StateKey,
} from "@angular/platform-browser";
import { of, Subject, Observable, BehaviorSubject, EMPTY } from "rxjs";
import { map, mergeMap, tap, catchError } from "rxjs/operators";

import {
  IWagtailResponse,
  IWagtailPage,
  IWagtailPageDetail,
  IWagtailModuleConfig,
  IWagtailRedirect,
  IPageTypes,
} from "./interfaces";
import { WAGTAIL_CONFIG } from "./tokens";

const PAGES_KEY = makeStateKey<string>("Wagtail Pages");
const CMS_DOMAIN_KEY = makeStateKey<string>("Wagtail Domain");
const CMS_SITE_ID_KEY = makeStateKey<string>("Wagtail Site ID");

@Injectable({
  providedIn: "root",
})
export class WagtailService {
  pageTypes = this.config.pageTypes;
  /* Every time a Wagtail Detail Page is retrieved this will emit the data. */
  cmsDetailPageSuccess$ = new Subject();
  pages$ = new BehaviorSubject(this.getPagesCache() || null);
  notFoundComponent = this.config.notFoundComponent;
  pages: IWagtailPage[] | null = null;
  private pagesApiPath = this.config.pagesApiPath
    ? this.config.pagesApiPath
    : "/api/v2/pages/";
  private pagesDraftApiPath = this.config.pagesDraftApiPath
    ? this.config.pagesDraftApiPath
    : undefined;
  private redirectsApiPath = "/api/redirects/";
  private limit = this.config.pagesApiLimit ? this.config.pagesApiLimit : 50;
  private fields = this.config.pagesApiFields
    ? this.config.pagesApiFields
    : "-first_published_at";
  private excludeApiTypes = this.config.excludeApiTypes
    ? this.config.excludeApiTypes
    : undefined;
  private wagtailSiteId = this.config.wagtailSiteId
    ? this.config.wagtailSiteId
    : null;

  /** Current path to find, this is used to keep state when lazy loading modules */
  pathToFind: string;

  constructor(
    private http: HttpClient,
    private location: Location,
    private state: TransferState,
    private title: Title,
    @Inject(DOCUMENT) private doc,
    private meta: Meta,
    private router: Router,
    @Inject(WAGTAIL_CONFIG) private config: IWagtailModuleConfig,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.pages$.subscribe((pages) => {
      this.pages = pages;
      if (pages && pages.length) {
        this.setPagesCache(pages);
      }
    });
  }

  /** Strip out query params and #anchor */
  sanitizePath(path: string) {
    return path.split("?")[0].split("#")[0];
  }

  /** Get current path without query params */
  private getPath() {
    return this.sanitizePath(this.location.path());
  }

  /** Find wagtail page that matches provided path */
  private findWagtailPageFromPath(wagtailPages: IWagtailPage[], path: string) {
    return wagtailPages.find((wagtailPage) => {
      const wagtailPagePath = this.normalizePath(wagtailPage.meta.html_url);
      return wagtailPagePath === path;
    });
  }

  /** Get angular wagtail page type conf from wagtail page */
  private getPageTypeFromPage(page: IWagtailPage) {
    return this.pageTypes.find((type) => type.type === page.meta.type);
  }

  /** Make composite cache key of cms + the path */
  private getPageDetailCache<T>(path) {
    const stateKey = makeStateKey("cms" + path);
    return this.state.get<T | null>(stateKey, null);
  }

  private setPageDetailCache(path, page: IWagtailPageDetail) {
    const stateKey = makeStateKey("cms" + this.normalizePath(path));
    this.state.set(stateKey, page);
  }

  private setPagesCache(pages) {
    this.state.set(PAGES_KEY, pages);
  }

  private getPagesCache() {
    return this.state.get<IWagtailPage[] | null>(PAGES_KEY, null);
  }

  /**
   * Attempt to find component for route. Return Observable<true> if found.
   * - Get all pages
   * - Add pages to router
   * - If page matches current path - navigate
   * - Else, get wagtail page by path
   * - If still not found, map to false
   */
  matchRouteToComponent(path: string) {
    path = this.normalizePath(path);
    this.pathToFind = path;
    return this.getPages().pipe(
      mergeMap((pages) => {
        const page = this.findWagtailPageFromPath(pages, path);
        if (page) {
          const pageType = this.getPageTypeFromPage(page);
          if (pageType) {
            this.pushToRouter(path, pageType);
            return of(true);
          }
        } else {
          return this.wagtailDetailByPath(path).pipe(
            mergeMap((pageDetail) => {
              const pageType = this.getPageTypeFromPage(pageDetail);
              if (pageType) {
                this.pushToRouter(path, pageType);
                this.setPageDetailCache(path, pageDetail);
                if (this.pages) {
                  this.setPages([...this.pages].concat(pageDetail));
                } else {
                  this.setPages([pageDetail]);
                }
                return of(true);
              }
              return of(false);
            }),
            catchError((err) => of(false))
          );
        }
        return of(false);
      })
    );
  }

  /**
   * Get the Wagtail Domain
   * Set the env var CMS_DOMAIN in node to override the domain
   * Otherwise it will default to "" meaning the same domain angular runs in.
   */
  getWagtailDomain() {
    let cmsDomain = this.state.get<string>(CMS_DOMAIN_KEY, "");
    if (cmsDomain) {
      return cmsDomain;
    }
    if (this.config.wagtailSiteDomain) {
      cmsDomain = this.config.wagtailSiteDomain;
    } else if (isPlatformServer(this.platformId)) {
      const envCmsDomain = process.env.CMS_DOMAIN;
      if (envCmsDomain) {
        cmsDomain = envCmsDomain;
      }
    }
    this.state.set(CMS_DOMAIN_KEY, cmsDomain);
    return cmsDomain;
  }

  /** Get Wagtail Site ID which may be configured by a Node env var or from the module config  */
  getWagtailSiteID() {
    let cmsId = this.state.get<number>(CMS_SITE_ID_KEY, null);
    if (typeof cmsId === "number") {
      return cmsId;
    }
    if (this.config.wagtailSiteId) {
      cmsId = this.config.wagtailSiteId;
    } else if (isPlatformServer(this.platformId)) {
      const envcmsId = process.env.WAGTAIL_SITE_ID;
      if (envcmsId) {
        cmsId = parseInt(envcmsId, 10);
      }
    }
    this.state.set(CMS_SITE_ID_KEY, cmsId);
    return cmsId;
  }

  getPages() {
    if (this.pages) {
      return of(this.pages);
    }
    const url = this.getWagtailDomain() + this.pagesApiPath;
    const siteId = this.getWagtailSiteID();
    const params = {
      limit: this.limit.toString(),
      fields: this.fields,
      ...(this.excludeApiTypes && { exclude_type: this.excludeApiTypes }),
      ...(siteId && {
        site: siteId.toString(),
      }),
    };
    return this.http
      .get<IWagtailResponse>(url, { params })
      .pipe(
        map((resp) => resp.items as IWagtailPage[]),
        tap((resp) => {
          if (this.pages) {
            this.setPages([...this.pages].concat(resp));
          } else {
            this.setPages(resp);
          }
        })
      );
  }

  /** Support for wagtail-headless-preview API */
  getPreviewData<T extends IWagtailPageDetail>(
    contentType: string,
    token: string
  ): Observable<T> {
    const url = this.getWagtailDomain() + "/api/v2/page_preview/1/";
    const params = new HttpParams({
      fromObject: {
        content_type: contentType,
        token: token,
        format: "json",
      },
    });
    return this.http.get<T>(url, { params });
  }

  /**
   * Look up page detail url from url path
   * If page is known already, directly request the detail route.
   * If not, try updating pages and finding the detail route
   * If still unable to find a matching route, error
   */
  private fetchPageForUrl<T extends IWagtailPageDetail>(
    path: string,
    params: { draft?: string }
  ): Observable<T> {
    let wagtailPage: IWagtailPage | undefined;
    if (this.pages) {
      wagtailPage = this.findPage(this.pages, path);
    }

    // Use known route if available
    if (wagtailPage) {
      const url = wagtailPage.meta.detail_url;
      return this.getWagtailPage<T>(url, params);
    } else {
      return this.getPages().pipe(
        mergeMap((pages) => {
          wagtailPage = this.findPage(pages, path);
          if (wagtailPage) {
            const url = wagtailPage.meta.detail_url;
            return this.getWagtailPage<T>(url, params);
          }
          return EMPTY;
        })
      );
    }
  }

  /**
   * Responsible for making sure the cache is hit where appropriate
   */
  getPageForUrl<T extends IWagtailPageDetail>(
    path: string,
    draftCode?: string | null,
    previewToken?: string,
    contentType?: string
  ): Observable<T> {
    // Get draft code if browser
    if (!draftCode && isPlatformBrowser(this.platformId)) {
      draftCode = new URL(location.href).searchParams.get("draft");

      // wagtail-headless-preview params
      if (!previewToken) {
        previewToken = new URL(location.href).searchParams.get(
          "wagtailPreviewToken"
        );
        contentType = new URL(location.href).searchParams.get(
          "wagtailContentType"
        );
      }
      if (previewToken && contentType) {
        return this.getPreviewData<T>(contentType, previewToken);
      }
    }

    // If there's a draftcode, never hit the cache
    if (draftCode) {
      return this.fetchPageForUrl<T>(path, { draft: draftCode });
    } else {
      const cmsData = this.getPageDetailCache<T>(this.normalizePath(path));

      if (cmsData) {
        return of(cmsData).pipe(tap(() => this.cmsDataSuccess(cmsData)));
      } else {
        return this.fetchPageForUrl<T>(path, {}).pipe(
          tap((page) => this.setPageDetailCache(path, page)),
          tap((page) => this.cmsDataSuccess(page))
        );
      }
    }
  }

  /** Get CMS data for current URL */
  getPageForCurrentUrl<T extends IWagtailPageDetail>(): Observable<T> {
    const path = this.getPath();
    return this.getPageForUrl<T>(path);
  }

  /** Happens when a Wagtail Detail Page is retrieved */
  cmsDataSuccess(cmsData: IWagtailPageDetail) {
    this.cmsDetailPageSuccess$.next(cmsData);
  }

  wagtailDetailByPath(path: string) {
    let draftCode: string | null = null;
    if (isPlatformBrowser(this.platformId)) {
      draftCode = new URL(location.href).searchParams.get("draft");
    }
    const wagtailSiteId = this.getWagtailSiteID();
    const params = {
      html_path: path,
      ...(wagtailSiteId && { site: wagtailSiteId.toString() }),
      ...(draftCode && { draft: draftCode }),
    };
    const url = this.getWagtailDomain() + this.pagesApiPath + "detail_by_path/";
    return this.http.get<IWagtailPageDetail>(url, { params });
  }

  getRedirect() {
    let path = this.getPath();
    if (path === "") {
      path = "/";
    }
    const siteId = this.getWagtailSiteID();
    const params = {
      old_path: path,
      ...(siteId && {
        site: siteId.toString(),
      }),
    };
    const url = this.getWagtailDomain() + this.redirectsApiPath;
    return this.http.get<IWagtailRedirect[]>(url, { params });
  }

  private findPage(pages: IWagtailPage[], path: string) {
    return pages.find(
      (page) =>
        new URL(page.meta.html_url).pathname.replace(/\/$/, "") ===
        path.replace(/\/$/, "")
    );
  }

  /** Add to router config only if not present */
  private pushToRouter(path: string, pageType: IPageTypes) {
    if (!this.router.config.find((conf) => conf.path === path)) {
      this.router.config.unshift({
        path,
        component: pageType.component,
        data: pageType.data,
        loadChildren: pageType.loadChildren,
        resolve: pageType.resolve,
        runGuardsAndResolvers: pageType.runGuardsAndResolvers,
      });
    }
  }

  normalizePath(path: string) {
    path = this.sanitizePath(path); // Remove query params
    if (path.startsWith("http")) {
      return new URL(path).pathname.replace(/\/$/, "").replace(/^\//, "");
    } else {
      return path.replace(/\/$/, "").replace(/^\//, "");
    }
  }

  /** Use the non cached admin_api when getting a draft */
  private getWagtailPage<T extends IWagtailPageDetail>(
    url: string,
    params: { draft?: string }
  ) {
    // Override api path if pagesDriftApiPath is set
    if (
      isPlatformBrowser(this.platformId) &&
      params.draft &&
      this.pagesDraftApiPath
    ) {
      url = url.replace(this.pagesApiPath, this.pagesDraftApiPath);
    }
    let httpParams = new HttpParams();
    if (params.draft) {
      httpParams = httpParams.set("draft", params.draft);
    }
    return this.http
      .get<T>(url, { params: httpParams })
      .pipe(tap((data) => this.setMeta(data)));
  }

  private setPages(pages: IWagtailPage[]) {
    // Remove duplicate pages
    this.pages$.next([
      ...new Map(pages.map((item) => [item.id, item])).values(),
    ]);
  }

  /** Set title and description meta tag for SEO */
  private setMeta(data: IWagtailPageDetail) {
    if (data.meta.seo_title) {
      this.title.setTitle(data.meta.seo_title);
    } else {
      this.title.setTitle(data.title);
    }
    if (data.meta.search_description) {
      this.meta.addTag({
        name: "description",
        content: data.meta.search_description,
      });
    }
    if (this.config.setCanonicalURL) {
      this.createLinkForCanonicalURL(data.meta.html_url);
    }
  }

  private createLinkForCanonicalURL(url: string) {
    const link: HTMLLinkElement = this.doc.createElement("link");
    link.setAttribute("rel", "canonical");
    this.doc.head.appendChild(link);
    link.setAttribute("href", url);
  }
}
