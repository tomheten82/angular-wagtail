import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { WagtailService } from "../wagtail.service";

@Component({
  selector: "lib-preview",
  templateUrl: "./preview.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreviewComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private wagtailService: WagtailService
  ) {}

  ngOnInit(): void {
    const queryParams = this.route.snapshot.queryParams;
    this.wagtailService
      .getPreviewData(queryParams.content_type, queryParams.token)
      .toPromise()
      .then((data) => {
        const url = new URL(data.meta.html_url);
        const newQueryParams = {
          wagtailPreviewToken: queryParams.token,
          wagtailContentType: queryParams.content_type,
        };
        this.router.navigate([url.pathname], {
          queryParams: newQueryParams,
        });
      });
  }
}
