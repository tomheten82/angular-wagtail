import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PreviewRoutingModule } from "./preview-routing.module";
import { PreviewComponent } from "./preview.component";
import { WagtailModule } from "../wagtail.module";

@NgModule({
  declarations: [PreviewComponent],
  imports: [CommonModule, PreviewRoutingModule],
})
export class PreviewModule {}
