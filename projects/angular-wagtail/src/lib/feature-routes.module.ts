import { NgModule, Inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Router } from "@angular/router";
import { FEATURE_PAGE_TYPES, WAGTAIL_CONFIG } from "./tokens";
import { IWagtailModuleConfig, IPageTypes } from "./interfaces";
import { WagtailService } from "./wagtail.service";

@NgModule({
  declarations: [],
  imports: [CommonModule]
})
export class FeatureRoutesModule {
  constructor(
    @Inject(FEATURE_PAGE_TYPES) pageTypesFeatures: IPageTypes[][],
    @Inject(WAGTAIL_CONFIG) config: IWagtailModuleConfig,
    router: Router,
    wagtailService: WagtailService
  ) {
    pageTypesFeatures.map(pageTypes => {
      pageTypes.map(pageType => {
        config.pageTypes.unshift(pageType);
      });
    });
    // Remove route that was used to load module
    router.config = router.config.filter(route => {
      return route.path !== wagtailService.pathToFind;
    });
  }
}
