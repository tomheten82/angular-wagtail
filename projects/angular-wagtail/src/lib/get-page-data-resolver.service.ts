import { Injectable } from "@angular/core";
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from "@angular/router";
import { IWagtailPageDetail } from "./interfaces";
import { WagtailService } from "./wagtail.service";

@Injectable({
  providedIn: "root",
})
export class GetPageDataResolverService implements Resolve<IWagtailPageDetail> {
  constructor(private wagtail: WagtailService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const draft = route.queryParamMap.get("draft");
    const wagtailPreviewToken = route.queryParamMap.get("wagtailPreviewToken");
    const wagtailContentType = route.queryParamMap.get("wagtailContentType");
    return this.wagtail.getPageForUrl(
      this.wagtail.sanitizePath(state.url),
      draft,
      wagtailPreviewToken,
      wagtailContentType
    );
  }
}
