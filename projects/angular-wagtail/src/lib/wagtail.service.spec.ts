import { TestBed, getTestBed } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { BrowserTransferStateModule } from "@angular/platform-browser";
import { of } from "rxjs";

import { WagtailService } from "./wagtail.service";
import { IWagtailPageDetail } from "./interfaces";
import { WAGTAIL_CONFIG } from "./tokens";

describe("WagtailService", () => {
  let injector: TestBed;
  let service: WagtailService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserTransferStateModule
      ],
      providers: [WagtailService, { provide: WAGTAIL_CONFIG, useValue: {} }]
    });
    injector = getTestBed();
    service = injector.get(WagtailService);
    httpMock = injector.get(HttpTestingController);
  });

  it("getPagesForUrl should make one http request", () => {
    const wagtailPage: IWagtailPageDetail = {
      id: 2,
      meta: {
        type: "wagtailcore.Page",
        detail_url: "http://localhost:4200/api/v2/pages/2/",
        html_url: "http://localhost:4200/",
        slug: "",
        first_published_at: "",
        search_description: "",
        seo_title: "",
        show_in_menus: false,
        parent: null
      },
      title: "Welcome to your new Wagtail site!"
    };
    service.getPages = () => of([wagtailPage]);
    const url = "/";
    service.getPageForUrl(url).subscribe();
    const mockReq = httpMock.expectOne(wagtailPage.meta.detail_url);
    mockReq.flush(wagtailPage);
    httpMock.verify();
  });
});
