import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SimpleLazyComponent } from "./simple-lazy.component";

const routes: Routes = [
  {
    path: "",
    component: SimpleLazyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimpleLazyRoutingModule {}
