import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SimpleLazyComponent } from "./simple-lazy.component";

@NgModule({
  declarations: [SimpleLazyComponent],
  imports: [CommonModule]
})
export class SimpleLazyModule {}
