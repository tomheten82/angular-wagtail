import { Component } from "@angular/core";

@Component({
  template: `
    <h1>404 Not Found</h1>
    <app-foo title="this is dah 404">LOL</app-foo>
  `
})
export class NotFoundComponent {}
