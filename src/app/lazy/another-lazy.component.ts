import { Component, OnInit } from "@angular/core";
import { tap } from "rxjs/operators";
import {
  WagtailService,
  IWagtailPageDetail
} from "../../../projects/angular-wagtail/src/public-api";

@Component({
  template: `
    <h2>Another Lazy Component</h2>
    <div>{{ cmsData?.title }}</div>
  `
})
export class AnotherLazyComponent implements OnInit {
  cmsData: IWagtailPageDetail;
  constructor(private wagtailService: WagtailService) {}

  ngOnInit() {
    this.wagtailService
      .getPageForCurrentUrl()
      .pipe(tap(page => (this.cmsData = page)))
      .toPromise();
  }
}
