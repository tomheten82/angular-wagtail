import { NgModule, ComponentFactoryResolver } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LazyComponent } from "./lazy.component";
import { AnotherLazyComponent } from "./another-lazy.component";
import { WagtailModule } from "../../../projects/angular-wagtail/src/public-api";
import { CoalescingComponentFactoryResolver } from "../../../projects/angular-wagtail/src/lib/coalescing-component-factory-resolver.service";

@NgModule({
  entryComponents: [AnotherLazyComponent, LazyComponent],
  declarations: [AnotherLazyComponent, LazyComponent],
  imports: [
    CommonModule,
    WagtailModule.forFeature([
      {
        component: LazyComponent,
        type: "sandbox.FooPage"
      },
      {
        component: AnotherLazyComponent,
        type: "sandbox.BarPage"
      },
    ])
  ]
})
export class LazyModule {
  constructor(
    coalescingResolver: CoalescingComponentFactoryResolver,
    localResolver: ComponentFactoryResolver
  ) {
    coalescingResolver.registerResolver(localResolver);
  }
}
